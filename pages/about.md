---
layout: page
title: About
permalink: /about/
---
# About

## Context

The reform of high schools introduces a new education followed by all students of general and technological second: [SNT (Digital Sciences and Technology)](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/08/5/spe641_annex_1063085.pdf). One of the topics covered in this course is the Internet of Things (IoT), which represents the extension of the Internet to things and places in the physical world.

The aim is to bring these young people to a first level of understanding of the Internet of Things. The challenge is to favor a chosen orientation, in this case towards digital engineering. The share of "digital" and "IT" in education has been greatly increased with the reform of the school.

## Goal

The goal of STM32Python project is to provide high school teachers and high school students with open-source teaching materials for initiation to the Internet of Things for the teaching of [SNT (Digital Sciences and Technology)](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/08/5/spe641_annex_1063085.pdf). These supports are based on the Nucleo STM32 platform from ST Microelectronics. They allow electronic assemblies and programs for STM32 microcontrollers to be made with the C / C ++ and microPython languages.

The supports produced can also be used by other general first and final lessons, in particular in NSI specialty (Digital and Computer Sciences), in IS specialty (Engineering Sciences), or in STI2D technological series (Sciences and Technologies of 'Industry and Sustainable Development).

## Partners
The partners of the STM32Python project are:
* les rectorats des académies de [Grenoble](http://www.ac-grenoble.fr) et d’[Aix-Marseille](http://www.ac-aix-marseille.fr),
* [ST Microelectronics](https://www.st.com),
* [Inventhys](http://www.inventhys.com),
* [Polytech Grenoble](https://www.polytech-grenoble.fr), [Grenoble INP Institut d'ingénierie et de management](https://www.grenoble-inp.fr/), [Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr).

## How to contribute?

Teacher, Student, High School Student, Engineer, Hobbyist, you have completed a supplement to these tutorials: do not hesitate to contact us to contribute to the project!

stm32python-contact@imag.fr

## Contributors
* Erwan LE SAINT
* Michael ESCODA
* Richard PHAN
* Romaric NOLLOT
* Guy CHATEIGNIER
* Didier DONSEZ
* Baptiste JOLAINE
* Aurélien REYNAUD
* Pedro LOPES
* Gaël LEMIERE
* Robin FARGES
* Florian VIOLET
* Leïla MICHELARD
* Manon CHAIX
* Gloria NGUENA