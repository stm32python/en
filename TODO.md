# TODOLIST

## General
* Choice of license for source codes (probably BSD-3)
* Choice of license for documents (probably one of the CC  https://creativecommons.org/licenses/?lang=en)
* ist contributors in `CONTRIBUTORS.md`
* add Wikipedia references in the glossary

## Tutorial
* Windows 10 Installation Guide
* Tutorial for [Fritzing](https://fritzing.org/home/)
* Tutorial for [Jupyter](https://jupyter.org/)
* Tutorial for Matlab
* Tutorial for daughter cards : MEMS, Teseo, DC Motor Driver, Stepper Driver ...
* Tutorial for individual components :
    * Keypad 4x3 https://air.imag.fr/index.php/Keypad
    * Nunchuck (in progress by Alex)
    * Super Nintendo NES Gamepad
    * Old fashion VU meter (to be connected to a PWN output of the card)
    * Infrared distance sensor
    * NFC SPI reader (ISO 14443, MiFare)
    * IR presence sensor (Motion detector)
* Mini-Robot tutorial with remote control app
    * accelerometer (shock) and magnetometer (trajectory correction) of the [X-NUCLEO-IKS01A3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html)
    * H-Bridge for 2 DC motors
    * 2WD Robotics Chassis 
    * Infrared distance sensor (Sharp type GP2Y0A41SK0F)
    * Time-of-flight distance sensor (VL53L0X, VL53L1X ...)
* Tutorial Weather station + Air quality
    * [X-NUCLEO-IKS01A3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html)
    * Weather Meter Kit
    * Grove Arduino shield
    * [RJ11 6-Pin Connector](https://www.sparkfun.com/products/132) x2
* G-Code tutorial for driving several NEMA stepper motors.
* Addition of the STM32WB55 card to the [MBlock IDE](https://ide.mblock.cc/?python#/) [see](https://www.mblock.cc/doc/en/developer-documentation/add-device-1.html)
* Tutorial of 4 x 7 segment clock or LCD with RTC and DCF77 synchronization
* Weather station tutorial (with LCD display) with the Meteo kit [https://www.sparkfun.com/products/15901]
* Air Quality Station tutorial see [https://github.com/airqualitystation] and [https://airqualitystation.github.io]
* Smart Doorbell tutorial: see [https://blog.st.com/stm32-roadshow/]
* Access control tutorial (NFC badger, infrared presence sensor, Buzzer, RTC ...)
* Tutorial Nunchuk + Pan-Tilt (2 servo-motors) + VU Meters (analog and LED)
* Tutorial Nucleo board 2 microphones + VU Meters (analog and LED)

## Firmware
* STM32WL55 (tutorial with TheThingsNetwork)
* [STM32 Nucleo B-L072Z-LRWAN1](https://www.st.com/en/evaluation-tools/b-l072z-lrwan1.html) (tutorial with TheThingsNetwork)

## Pages
* Generate favicons from the logo
* Added menu for language change
* Addition of a grid for the home page (Masonry https://masonry.desandro.com/)
* Adding tags to pages and making a tags page 
* Added SEO tags  https://github.com/jekyll/jekyll-seo-tag
* Add STM32Python logo
* Add permalinks in tutorial indexes
* Add a site about privacy (site.privacy in _config.yml)
* Add feed.xml?

## Writing conventions
* file name and images in lowercase
