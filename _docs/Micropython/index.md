---
title: µPython and STM32WB
description: µPython and STM32WB

---
# MicroPython and STM32WB

<p>
<img align="center" src="images/upython.png" alt="upython" width="100"/>
<img align="center" src="images/st2.png" alt="stm" width="250"/>
</p>

You will find in this part all MicroPython tutorials.

First of all, make sure you have a functional installation.
The protocol to follow is detailed in the 2 start-up guides.

## Summary

* [What is MicroPython ?](micropython)
* [Windows 10 Quick Start Guide](install_win10)
* [Linux Quick Start Guide](install_linux)
* [The STM32WB55 development kit](stm32wb55)
* [Basic examples to use MicroPython](exercices)
* [Basic examples to use MicroPython with the IKS01A3 expansion card](iks01a3)
* [Basic examples to use MicroPython with Grove sensors](grove)
* [Using BLE with a smartphone](ble)
* [Creating a Smartphone Application with MIT App Inventor](AppInventor)
* Programming of a mini-robot and control of its DC motors from a Smartphone (soon)
* [Frequently Asked Questions](faq)
* [Glossary](glossaire)
* [Useful links](ressources)
