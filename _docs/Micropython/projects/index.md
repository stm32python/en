---
title: Projects and applications with STM32python
Description: List of projects and applications with STM32python
---

# Projects and applications with STM32python


## [Weather Station](https://stm32python.gitlab.io/fr/assets/projects/station_meteo.zip)

Prototyping a weather station with a NUCLEO-WB55 inspired by the Ikea Klockis product. **Thanks to Christophe Priouzeau & Gérald Hallet! **
