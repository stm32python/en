---
title: Horloge temps-réel
description: Mise en oeuvre de la Real Time Clock (RTC) du STM32WB55 en MicroPython
---

# Real Time Clock

This tutorial explains how to implement the real time clock of the STM32WB55 with MicroPython.

The real-time clock (RTC) is an electronic circuit integrated into the STM32WB55 microcontroller fulfilling the functions of a very precise clock for time-stamping issues, management of alarms triggered according to a calendar and able to put the microcontroller to sleep or wake it up, etc. In order to be as precise and stable as possible, the RTC can use a 32kHz vibrating quartz as a frequency source.

## Usage

The RTC is quite simple to handle, since we'll only use one method : `pyb.RTC().datetime()`.
The date used will then take the following format :
- \[y,  m,  j,  wd,  h,  m,  s,  sub]
- year  month  day  day_of_the_week  hours  minutes  seconds  subsecond(internal counter)

We'll begin to give a name to the RTC
```python
rtc = pyb.RTC()
```
`rtc.datetime()` used alone it returns the current date at shown format.
`rtc.datetime(date)` replace the current date.
The date of Saturday, January 2, 2021 at 12:21 51s can the be defined as follows :
```python
rtc.datetime((2021, 1, 2, 6, 12, 21, 51, 0))
```
and it can be retrieved like this :
```python
date = rtc.datetime()
```

## Examples

So a first code to display on the terminal would be :
```python
# Purpose of the script : Implementation of the real time clock of the STM32WB55

import time # delay library

# we declare the RTC
rtc = pyb.RTC()

# year  month  day  day_of_the_week  hours  minutes  seconds  subsecond(internal counter)
#	   year  m  j  wd h  m  s  sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
#index :   0  1  2  3  4  5  6  7

# we initialize the date
rtc.datetime(date)

while True :
	# we retrieve the updated date
	date = rtc.datetime()
	# and we display it
	print('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+'min'+'{0:02d}'.format(date[6])+'s')
	# we refresh every second
	time.sleep(1)
```
We'll notice the use of `'{0:02d}'.format(date[4])` that allow to display _02_ instead of _2_.

With the help of the [8x7-segments TM1638 Display tutorial](https://stm32python.gitlab.io/en/docs/Micropython/tm1638) a clock with LED display can be made with the following example code:
```python
# Purpose of the script : Implementation of the real time clock of the STM32WB55
# Creation of an LED clock with an 8x7-segment display TM1638.

import tm1638
from machine import Pin
import time

# we declare the display's card
tm = tm1638.TM1638(stb=Pin('D2'), clk=Pin('D3'), dio=Pin('D4'))

# we declare the RTC
rtc = pyb.RTC()

# we reduce the brightness
tm.brightness(0)

# year  month  day  day_of_the_week  hours  minutes  seconds  subsecond(internal counter)
#	   year  m  j  wd h  m  s  sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
# index :  0  1  2  3  4  5  6  7

# we initialize the date
rtc.datetime(date)

while True :
	# we retrieve the updated date
	date = list(rtc.datetime()) # rtc.datetime() returns a tuble object, we turn it into a list to modify it
	
	# and we display it, making the second dot blinking
	if (date[6] % 2) :
		tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' .'+'{0:02d}'.format(date[6]))
	else :
		tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' '+'{0:02d}'.format(date[6]))
	
	# we retrieve information on the buttons to adjust time
	buttons = tm.keys()
	
	# we compare bit to bit to identify buttons
	if (buttons & 1) :
		date[4] += 1
		if (date[4] > 23) :
			date[4] = 0
		rtc.datetime(date)
	if (buttons & 1<<1) :
		date[5] += 1
		if (date[5] > 59) :
			date[5] = 0
		rtc.datetime(date)
	if (buttons & 1<<2) :
		date[6] += 1
		if (date[6] > 59) :
			date[6] = 0
		rtc.datetime(date)
	
	# we refresh every 100 ms (for a more sensitive read of the buttons it can be changed)
	time.sleep_ms(100)
```
