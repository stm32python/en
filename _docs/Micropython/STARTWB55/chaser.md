---
title: Program a chaser
description: Tutorial to program a chaser with MicroPython
---

# Program a chaser

This tutorial explains how to blink *in a cyclic sequence* the three light emitting diodes on the NUCLEO-WB55 board with MicroPython. This is called a "chaser".

## Required tools

The NUCLEO-WB55 board. We will blink its 1,2 and 3 LEDs:

<div align="center">
<img alt="LEDS" src="images/leds.jpg" width="500px">
</div>

Here is the organization of LEDs by color and number:

1. LED 1: Blue
2. LED 2: Green
3. LED 3: Red

## MicroPython code

**The following scripts are available in the [download area](https://stm32python.gitlab.io/fr/assets/Script/NUCLEO_WB55.zip)**.

With MicroPython, the **pyb.LED** module allows you to manage LEDs very simply.
Edit the *main.py* script in the NUCLEO-WB55 virtual USB disk directory: *PYBFLASH*.

``` python
# Script object: program a chaser
# Example of GPIO configuration for NUCLEO-WB55 integrated LED management

import pyb # For device access (GPIO, LED, etc.)
from time import sleep_ms # For system breaks

print( "Les LED avec MicroPython c'est facile" )

# Initialisation des LED
led_blue = pyb.LED(3) # LED1 screen printed on the PCB
led_green = pyb.LED(2) # LED2 screen printed on the PCB
led_red = pyb.LED(1) # LED3 screen printed on the PCB

# Initialization of the LED Counter 
led_counter = 0

while True: # Creation of an "infinite" loop (no output clause)
	
	if led_counter == 0:
		led_blue.on()
		led_red.off()
		led_green.off()
	elif led_counter == 1:
		led_blue.off()
		led_green.on()
		led_red.off()
	else :
		led_blue.off()
		led_green.off()
		led_red.on()
		
	# We want to turn on the next LED at the next loop iteration
	led_counter = led_counter + 1
	if led_counter > 2:
		led_counter = 0
	
	sleep_ms(500) # 500ms Timeout
```

You can start the script with Ctrl + D on the Putty terminal and observe the 3 leds that light up and then turn off according to the sequence LED3 -> LED2 -> LED1 -> LED3 -> LED2 ...

## Going Further 

You will find [here](button_it) a variant of this code that explains how to add an interaction with a button using the *interruptions* mechanism.
