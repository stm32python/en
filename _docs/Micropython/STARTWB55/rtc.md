---
title: Real Time Clock
description: Implementation of the Real Time Clock (RTC) of the STM32WB55 in MicroPython
---

# Real Time Clock

This tutorial explains how to implement the STM32WB55 real-time clock in MicroPython.

The Real-Time Clock (RTC) is an electronic circuit integrated in the STM32WB55 microcontroller that performs the functions of a very precise clock for timestamp problems, for alarm management which are triggered according to a calendar and which are capable of putting the microcontroller in sleep or waking it up, etc. In order to be as precise and stable as possible, the RTC can use a vibrating quartz crystal at 32kHz as a frequency source.

## Use

The RTC is quite simple to use since only one method will serve us: `pyb.RTC(). datetime()`.
The date used will then take the following format:
-  [year,  m,  j,  wd,  h,  m,  s,  sub]
- year month day-of-the-month hour minute second subsecond (internal counter)

We will start by giving a name to the RTC
``python
rtc = pyb.RTC()
```
`rtc.datetime()` that is used only returns the current date in the specified format.
`rtc.datetime(date)` replaces the current date.
The date of Saturday, January 2, 2021 at 12:21:51 can then be defined as follows:
```python
rtc.datetime((2021, 1, 2, 6, 12, 21, 51, 0))
```
and it can be retrieved like this:
```python
date = rtc.datetime()
```

## Examples

**The following scripts are available in [download area](https://stm32python.gitlab.io/fr/assets/Script/NUCLEO_WB55.zip)**.

Thus a first code to display on the terminal would be:
```python
# Objet of the script : Setting up the STM32WB55 Real Time 
# Clock (RTC)

import time # Timer library

# declaring the RTC
rtc = pyb.RTC()

# year month day day_the_semaine hour minute second subsecond(internal counter)
#	   year  m  j  wd h  m  s  sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
#indices : 0  1  2  3  4  5  6  7

# initializing the date
rtc.datetime(date)

while True :
	# retrieving the updated date
	date = rtc.datetime()
	# and displaying it
	print('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+'min'+'{0:02d}'.format(date[6])+'s')
	#  we update every second
	time.sleep(1)
```
Note the use of `{0:02d}'. format(date[4])' which allows the display of _02_ rather than _2_.

Using the [tutorial of the 8x7-segment display TM1638](../grove/tm1638) you can make a clock with LED display with the following example code:

```python
# Objet of the script : Setting up the STM32WB55 Real Time 
# Clock (RTC)
# Creation of an LED clock using a 8x7-segment display TM1638.

import tm1638
from machine import Pin
import time

# declaring the display card
tm = tm1638.TM1638(stb=Pin('D2'), clk=Pin('D3'), dio=Pin('D4'))

# declaring the <rtc>)
rtc = pyb.RTC()

# reducing brightness
tm.brightness(0)

# year month day day_the_semaine hour minute second subsecond(internal counter)
#	   year  m  j  wd h  m  s  sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
#indices : 0  1  2  3  4  5  6  7

# initializing the date
rtc.datetime(date)

while True :
	# retrieving the update date
	date = list(rtc.datetime()) # rtc.datetime() returns a non modifiable object, we change it to a list to modify it
	
	# and it is displayed by blinking the point for a few seconds
	if (date[6] % 2) :
		tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' .'+'{0:02d}'.format(date[6]))
	else :
		tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' '+'{0:02d}'.format(date[6]))
	
	# the information on the buttons is retrieved to set the time
	boutons = tm.keys()
	
	# we compare bit to bit to identify the button
	if (boutons & 1) :
		date[4] += 1
		if (date[4] > 23) :
			date[4] = 0
		rtc.datetime(date)
	if (boutons & 1<<1) :
		date[5] += 1
		if (date[5] > 59) :
			date[5] = 0
		rtc.datetime(date)
	if (boutons & 1<<2) :
		date[6] += 1
		if (date[6] > 59) :
			date[6] = 0
		rtc.datetime(date)
	
	#  Refreshing every 100 milliseconds (for more sensitive button playback, can be modified)
	time.sleep_ms(100)
```
