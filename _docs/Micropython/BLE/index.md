---
title: Using BLE with the NUCLEO-WB55 board
description: UUsing BLE with the NUCLEO-WB55 board
---

# Using BLE with the NUCLEO-WB55 board

## Bluetooh Low Energy (BLE) Presentation

A presentation of the Bluetooh Low Energy (BLE) is available on [this page](ble). Take the time to assimilate this information because the BLE protocols are subtly different from the more common "client-server" and "master-slave" logics and introduce a vocabulary and notions that we use in all the tutorials that follow.

## List of tutorials 

The following links give some examples of BLE applications with the NUCLEO-WB55 card. We remind you that all the scripts presented or used later can be retrieved from the [**Downloads page**](../Downloads) in addition to the links present throughout the text.

**Attention**, there are still few examples on the BLE with MicroPython and the libraries concerned are still under development. The subject of writing classes driving BLE radio is therefore addressed to knowledgeable users. However, you should have no trouble adapting the provided scripts to your applications if they do not require a change in BLE management.

The *Central* column specifies whether the latter is a NUCLEO-WB55 or a smartphone equipped with either the ST BLE Sensor application or an Android application made with MIT App Inventor. The *Peripheral* is ALWAYS a NUCLEO-WB55. Among other possibilities, the central office can also be a personal computer (MAC or PC) or a Raspberry Pi microcomputer running the Linux operating system. Corresponding examples are sure to be added in the future.

<br>
<br>

|**Central**|**Peripheral**|**Protocole**|**Description and hyperlink**|**Standard**|
|:-|:-:|:-:|:-:|:-:|
ST BLE Sensor (smartphone)|NUCLEO-WB55|GAP then GATT|[Temperature publication and LED control](STBLESensor)|Blue-ST|
ST BLE Sensor (smartphone)|NUCLEO-WB55|GAP then GATT|[Environnemental Station](StationEnvBlueST)|Blue-ST|
MIT App Inventor (smartphone)|NUCLEO-WB55|GAP then GATT|[Temperature publication and LED control](MITAppInventor_1)|Blue-ST|
MIT App Inventor (smartphone)|NUCLEO-WB55|GAP then GATT|[Temperature and humidity publicationl](MITAppInventor_2)|Bluetooth SIG|
MIT App Inventor (smartphone)|NUCLEO-WB55|GAP then GATT|[String exchange](MITAppInventor_3)|Nordic UART Service|
NUCLEO-WB55|NUCLEO-WB55|GAP|[Sending and reading offline messages](BLEGAP)|Bluetooth SIG|
NUCLEO-WB55|NUCLEO-WB55|GAP then GATT|[Temperature and humidity exchange](BLESIG)|Bluetooth SIG|
NUCLEO-WB55|NUCLEO-WB55|GAP then GATT|[String exchange](BLEUART)|Nordic UART Service|
