---
title: Stm32duino
description: Section Stm32duino

---
# Stm32duino

[Stm32duino](https://github.com/stm32duino) is a project bringing together Arduino libraries for STM32 development boards (Nucleo and Discovery) and for MEMS components from STMicroelectronics. It allows to develop and compile programs (eg _sketch_) from the Arduino IDE development environment.

# Summary

You will find in this part all the [Stm32duino](https://github.com/stm32duino) exercises for the pedagogical kit Stm32Python.

First of all, be sure to have a functional installation.
The protocol to follow is detailled in the [Installation](installation) section.

 - [Installation](installation)
 - [Exercises](exercises)
