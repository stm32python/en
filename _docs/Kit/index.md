---
title: STM32MicroPython Kit
description: Description of the STM32MicroPython educational kit
categories: stm32 kit nucleo python
---

## STM32 boards

STMicroelectronics STM32 Nucleo and Discovery boards can be programmed in C/C ++ from
* the [Arduino IDE](https://github.com/stm32duino/wiki/wiki/Getting-Started)
* the [STM32Cube IDE](https://www.st.com/stm32cube)
* the [MBed](https://os.mbed.com/platforms/ST-Nucleo-F446RE/) online editor
* the [RIOT OS](https://github.com/RIOT-OS/RIOT/tree/master/boards/nucleo-f446re) toolchain

F4xx models can be programmed in [microPython](https://micropython.org/stm32/).

Some models of STM32 are programmable through MicroPython.
The P-NUCLEO-WB55 educational kit is one of them.

## The P-NUCLEO-WB55 educational kit

The STM32 Python educational kit includes the following cards:

* [P-NUCLEO-WB55 Bluetooth ™ 5 and 802.15.4 Nucleo Pack including USB dongle and Nucleo-68 with STM32WB55 MCUs](https://www.st.com/en/evaluation-tools/p-nucleo-wb55. html) (available from [Farnell](https://uk.farnell.com/stmicroelectronics/p-nucleo-wb55/carte-iot-solut-mcu-arm-cortex/dp/2989052)).

* [X-NUCLEO-IKS01A3 - Expansion board, MEMS motion sensor and environment for STM32 Nucleo, Arduino UNO Layout R3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html) (available from [Farnell](https://uk.farnell.com/stmicroelectronics/x-nucleo-iks01a3/carte-d-extension-stm32-nucleo/dp/3106035)).

* [Grove connectors board](http://wiki.seeedstudio.com/Grove_System/#grove-starter-kit) for connecting [Grove](http://wiki.seeedstudio.com/Grove_System/) sensors and extra sensors (potentiometer , ultrasound, thermometer, LED display, infrared receivers, LCD display ...) (on sale at [Farnell](https://uk.farnell.com/seeed-studio/83-16991/grove-starter-kit-for-arduino/dp/2801858?st=Grove) and at [Seeedstudio](https://www.seeedstudio.com/Base-Shield-V2.html)). _This card is recommended but it is not mandatory._


| | | |
| - | - | - |
![p-nucleo-wb55](images/p-nucleo-wb55.jpg) | ![x-nucleo-iks01a3](images/x-nucleo-iks01a3.jpg) | ![grove_starter_kit](images/grove_starter_kit.jpg)

Image credits:
* [STMicroelectronics](https://www.st.com)
* [Seeedstudio](http://wiki.seeedstudio.com/Grove_System)

## Where to buy this kit ?

STM32 Nucleo boards from the SNT educational kit can be purchased online or by order form from electronic component suppliers such as [Farnell](https://www.farnell.com), [RS Online](https://www.rs-online.com/web/), [Conrad](https://www.conrad.fr), [Digikey](https://www.digikey.com/) or [Mouser](https://www.mouser.com/).

## Other kits

Note: other kits are possible for STM32MicroPython tutorials. [See the list ...](Kits)


Image credits:
* [ST Microelectronics](https://www.st.com)
* [Seeedstudio](http://wiki.seeedstudio.com/Grove_System)